module rip.io.interfaces;

import std.stdio;

interface FormatWorker(T) {
    T save(in T surface, in string name) const;
    //save не влияет на дальнейшую работу программы
    //т.е. можно вернуть surface после сохранения для UFCS
    //ошибка должна выбивать исключение

    T load(in string name) const;
    T decode(File file) const;
    bool checkOnHeader(File file) const;
    //как нам получить результат ошибки?
}

//этому шаблону здесь не место
mixin template colorsToFile() {
    import std.stdio;
    void toFile(RGBColor color)  {
        file.write(
                    color.red!char,
                    color.green!char,
                    color.blue!char);
    }

    /*surface.getPixelsRange().each!toFile;*/
    auto range = surface.getPixels();

    import std.algorithm;
    /*each!toFile(range);*/
    //Бага?
    //source/io/interfaces.d(25,16): Error: function declaration without return type. (Note that constructors are always named 'this')
    //source/io/interfaces.d(25,23): Error: no identifier for declarator each!toFile(range)
}
