module rip.processing;

public {
    import rip.processing.filters;
    import rip.processing.grayscale;
    import rip.processing.convolution;
    import rip.processing.negative;
}
